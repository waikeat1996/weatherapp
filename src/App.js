import React from 'react'
import './App.css'
import 'weather-icons/css/weather-icons.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import Weather from './app_component/weather.component'
import Form from './app_component/form.component'

const API_key = 'c12874813a72b70b020a1c57ed1c12a3'

class App extends React.Component {
  render () {
    return (
      <div className='App'>
        <Form />
      </div>
    )
  }
}

export default App
