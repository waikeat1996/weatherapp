import React from 'react'
import './form.style.css'
import { useState, useEffect } from 'react'
import Weather from './weather.component'

const API_key = 'ebae7592ba253558fe04a9dc8d5f7d02'

const Form = (props) => {
  const weatherIcon = {
    Thunderstorm: 'wi-thunderstorm',
    Drizzle: 'wi-sleet',
    Rain: 'wi-storm-showers',
    Snow: 'wi-snow',
    Atmosphere: 'wi-fog',
    Clear: 'wi-day-sunny',
    Clouds: 'wi-day-fog',
  }

  const [
    weatherObj,
    setWeatherObj,
  ] = useState([])

  const [
    error,
    setError,
  ] = useState([
    false,
  ])

  const [
    textEmpty,
    setTextEmpty,
  ] = useState([
    false,
  ])

  const [
    url,
    setUrl,
  ] = useState([])

  const [
    city,
    setCity,
  ] = useState([
    '',
  ])

  const loadMoreCommit = () => {
    setUrl(
      'http://api.openweathermap.org/data/2.5/weather?q=' +
        city +
        '&appid=' +
        API_key,
    )
  }

  const handleClick = (id) => {
    setWeatherObj(weatherObj.filter((item) => item.id !== id))
  }

  useEffect(
    () => {
      console.log('test ', city)
      fetch(url, {
        method: 'GET',
        headers: new Headers({
          Accept: 'application/vnd.github.cloak-preview',
        }),
      })
        .then((res) => res.json())
        .then((response) => {
          console.log('test ', response)
          if (response.cod !== 200) {
            console.log(' error')
            setError(true)
          } else if (response.cod === 200) {
            console.log('no error')
            setError(false)
            setWeatherObj(weatherObj.concat(response))
          }
        })
        .catch((error) => console.log('error ', error))
    },
    // eslint-disable-next-line
    [
      url,
    ],
  )

  return (
    <div className='container'>
      <div>{error === true ? displayError() : null}</div>
      <div>{textEmpty === true ? displayTextEmpty() : null}</div>
      <form onSubmit={SetText}>
        <div className='row'>
          <div className='col-md-3 offset-md-4'>
            <input
              type='text'
              className='form-control'
              name='city'
              autoComplete='off'
              placeholder='City'
              autoFocus
              onChange={(event) => setCity(event.target.value)}
            />
          </div>

          <div className='col-md-3 mt-md-0  text-md-left'>
            <button className='btn btn-warning'>Add Weather</button>
          </div>
        </div>

        <div style={{ display: 'inline-block', width: '1000px' }}>
          {weatherObj.length !== 0 ? (
            weatherObj.map((item, index) => (
              <div
                key={index}
                style={{
                  display: 'inline-block',
                  paddingLeft: 50,
                  paddingRight: 50,
                }}
              >
                <Weather
                  city={item.name}
                  temp_celsius={calCelsius(item.main.temp)}
                  temp_max={calCelsius(item.main.temp_max)}
                  temp_min={calCelsius(item.main.temp_min)}
                  description={item.weather[0].description}
                  weatherIcon={getWeatherIcon(weatherIcon, item.weather[0].id)}
                  id={item.id}
                  handleClick={handleClick}
                />
              </div>
            ))
          ) : null}
        </div>
      </form>
    </div>
  )

  function calCelsius (temp) {
    let cell = Math.floor(temp - 273.15)
    return cell
  }

  function getWeatherIcon (icons, rangeId) {
    switch (true) {
      case rangeId >= 200 && rangeId <= 232:
        return weatherIcon.Thunderstorm
      case rangeId >= 300 && rangeId <= 321:
        return weatherIcon.Drizzle
      case rangeId >= 500 && rangeId <= 531:
        return weatherIcon.Rain
      case rangeId >= 600 && rangeId <= 622:
        return weatherIcon.Snow
      case rangeId >= 701 && rangeId <= 781:
        return weatherIcon.Atmosphere
      case rangeId === 800:
        return weatherIcon.Clear
      case rangeId >= 801 && rangeId <= 804:
        return weatherIcon.Clouds
      default:
        return weatherIcon.Clouds
    }
  }

  function SetText (e) {
    e.preventDefault()
    if (e.target.elements.city.value) {
      setTextEmpty(false)
      loadMoreCommit()
    } else {
      setTextEmpty(true)
    }
  }
}

function displayError () {
  return (
    <div className='alert alert-danger mx-5' role='alert'>
      ErrorOccur
    </div>
  )
}

function displayTextEmpty () {
  return (
    <div className='alert alert-danger mx-5' role='alert'>
      Please Enter City
    </div>
  )
}

export default Form
