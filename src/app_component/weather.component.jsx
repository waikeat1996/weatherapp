import React from 'react'
import { useState } from 'react'

const Weather = (props) => {
  const [
    isShown,
    setIsShown,
  ] = useState(false)

  return (
    <div
      className='container '
      onMouseOver={() => setIsShown(true)}
      onMouseLeave={() => setIsShown(false)}
    >
      <div className='cards pt-4'>
        <h1>{props.city}</h1>
        <h5 className='py-4'>
          <i className={'wi ' + props.weatherIcon + ' display-1'} />
        </h5>
        {props.temp_celsius ? (
          <h1 className='py-2'>{props.temp_celsius}&deg;C</h1>
        ) : null}

        <h4 className='py-3'>{props.description}</h4>
        {isShown && (
          <button type='button' onClick={() => props.handleClick(props.id)}>
            Remove
          </button>
        )}
      </div>
    </div>
  )
}

export default Weather
